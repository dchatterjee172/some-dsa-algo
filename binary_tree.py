import typing
import random
import json


class Node:
    def __init__(self, value: typing.Any):
        self.value = value
        self.left = None
        self.right = None
        self.parent = (None, None)  # parent Node, Node is in parent's left?

    def __gt__(self, other):
        return self.value > other.value

    def __eq__(self, other):
        return self.value == other.value

    def __str__(self):
        return json.dumps(self.to_dict())

    def to_dict(self):
        return dict(
            value=self.value,
            left=self.left.value if self.left is not None else None,
            right=self.right.value if self.right is not None else None,
            parent=self.parent[0].value if self.parent[0] is not None else None,
        )

    def assign_child(self, node, go_left: bool):
        if go_left:
            self.left = node
        else:
            self.right = node
        if node is not None:
            node.parent = (self, go_left)


class BinaryTree:
    def __init__(self, items: typing.Optional[typing.Iterator[typing.Any]]):
        self.root = None
        self.count = 0
        if items is not None:
            self.update(items)

    def get_parent(self, node, start_here=None):
        current = self.root if start_here is None else start_here
        while True:
            if current > node:
                if current.left is None:
                    return current, True
                else:
                    current = current.left
            elif node > current:
                if current.right is None:
                    return current, False
                else:
                    current = current.right
            else:
                return None, None

    def update(self, items: typing.Iterator[typing.Any]):
        if len(items) == 0:
            return self
        if self.root is None:
            self.root = Node(items[0])
        for item in items[1:]:
            item = Node(item)
            parent, go_left = self.get_parent(item)
            if parent is None:
                continue
            item.parent = (parent, go_left)
            if go_left:
                parent.left = item
            else:
                parent.right = item

    def inorder(self) -> typing.Iterator[Node]:
        if self.root is None:
            return []
        stack = [self.root]
        visited = set()
        while len(stack) > 0:
            current = stack.pop()
            if current.left is None or current.value in visited:
                yield current
                if current.right is not None:
                    stack.append(current.right)
            else:
                stack.append(current)
                stack.append(current.left)
            visited.add(current.value)

    def preorder(self) -> typing.Iterator[Node]:
        if self.root is None:
            return []
        stack = [self.root]
        while len(stack) > 0:
            current = stack.pop()
            yield current
            if current.right is not None:
                stack.append(current.right)
            if current.left is not None:
                stack.append(current.left)

    def get_node(self, value: typing.Any) -> Node:
        item = Node(value)
        current = self.root
        node = None
        while current is not None:
            if current > item:
                current = current.left
            elif item > current:
                current = current.right
            else:
                node = current
                break
        return node

    def make_root(self, node):
        self.root = node
        node.parent = (None, None)

    def discard(self, value: typing.Any):
        node = self.get_node(value)
        if node is None:
            return
        parent, go_left = node.parent
        if parent is not None:
            parent.assign_child(None, go_left)
        if node.left is None and node.right is None:
            parent.assign_child(None, go_left)
        elif node.left is None and node.right is not None:
            if parent is None:
                self.make_root(node.right)
            else:
                parent.assign_child(node.right, go_left)
        elif node.left is not None and node.right is None:
            if parent is None:
                self.make_root(node.left)
            else:
                parent.assign_child(node.left, go_left)
        else:
            left = node.left
            if parent is None:
                self.make_root(left)
            else:
                parent.assign_child(left, go_left)
            right = node.right
            right_parent, right_go_left = self.get_parent(right, start_here=parent)
            right_parent.assign_child(right, right_go_left)

        del node
        self.count -= 1

    def __str__(self):
        if self.root is None:
            return ""
        return json.dumps([v.to_dict() for v in self.inorder()], indent=2)

    def __len__(self):
        return self.count


def test():
    for i in range(100):
        values = list(set(random.randint(-100000, 100000) for _ in range(2000)))
        tree = BinaryTree(values)
        assert [node.value for node in tree.inorder()] == sorted(values)
        for i in (random.randint(0, 100) for _ in range(500)):
            tree.discard(values[i])
            del values[i]
        inorder = [node.value for node in tree.inorder()]
        assert inorder == sorted(values), inorder == sorted(inorder)


if __name__ == "__main__":
    values = list(random.randint(-15, 15) for _ in range(10))
    print(values)
    tree = BinaryTree(values)
    preorder = [node.value for node in tree.preorder()]
    print(preorder)
