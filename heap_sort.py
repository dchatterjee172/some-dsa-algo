import typing
import math
import random


class Node:
    def __init__(self, value: typing.Any):
        self.value = value

    def __gt__(self, other):
        return self.value > other.value

    def __str__(self):
        return str(self.value)


class Heap:
    def __init__(self, items: typing.Optional[typing.Iterable] = None):
        self.nodes = []
        if items is not None:
            self.update(items)

    def update(self, items: typing.Iterable):
        if len(items) == 0:
            return
        for item in items:
            item = Node(item)
            self.nodes.append(item)
            current = len(self) - 1
            while current > 0:
                parent = math.ceil(current / 2) - 1
                if self.nodes[parent] < self.nodes[current]:
                    temp = self.nodes[parent]
                    self.nodes[parent] = self.nodes[current]
                    self.nodes[current] = temp
                    current = parent
                else:
                    break

    def pop_root(self):
        root = self.nodes.pop(0)
        try:
            last = self.nodes.pop()
        except IndexError:
            return root
        self.nodes.insert(0, last)
        current = 0
        while current < len(self.nodes):
            left = (current + 1) * 2 - 1
            right = (current + 1) * 2
            if left < len(self) and right < len(self):
                child = left if self.nodes[left] > self.nodes[right] else right
            elif left < len(self):
                child = left
            else:
                child = right
            if child < len(self) and self.nodes[child] > self.nodes[current]:
                temp = self.nodes[child]
                self.nodes[child] = self.nodes[current]
                self.nodes[current] = temp
                current = child
            else:
                break
        return root

    def __str__(self):
        return ", ".join(str(node) for node in self.nodes)

    def __len__(self):
        return len(self.nodes)


def test():
    for i in range(10000):
        items = [random.randint(-1000, 1000) for _ in range(100)]
        heap = Heap(items)
        sort_ = []
        while len(heap) > 0:
            sort_.append(heap.pop_root().value)
        assert sort_ == sorted(items, reverse=True)


if __name__ == "__main__":
    items = [1, 2, 3, 4, 5, 6, -1]
    print(items)
    heap = Heap(items)
    print(heap)
    print()
    while len(heap) > 0:
        print(heap.pop_root())
    test()
